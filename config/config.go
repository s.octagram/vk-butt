package config

type Config struct {
	DefaultBaseURL string
	DefaultVersion string
	DefaultLang string
}

func Init() (*Config) {
	config := Config{
		DefaultBaseURL: "https://api.vk.com/method",
		DefaultVersion: "5.103",
		DefaultLang: "en",
	}
	return &config
}