package longpoll

import (
	"gitlab.com/s.octagram/vk-butt"
	"gitlab.com/s.octagram/vk-butt/longpoll"
)

type Mode int64

const (
	ReceiveAttachments           Mode = 2
	ReturnExpandedSetOfEvents    Mode = 8
	ReturnPts                    Mode = 32
	ReturnFriendOnlineExtraField Mode = 64
	ReturnRandomID               Mode = 128
)

const (
	eventHistoryOutdated = iota + 1
	keyExpired
	userInformationLost
	invalidVersion
)

type Longpoll struct {
	vk *vkbutt.VkButt

	Key     string
	Server  string
	Wait    int64
	Mode    Mode
	Version int64
	NeedPts int64
	GroupID int64
} 

func (lp *Longpoll) ServerUpdate() (int64, error) {
	request := longpoll.RequestParams{
		"need_pts": lp.NeedPts,
		"lp_version": lp.Version,
	}

	if lp.GroupID > 0 {
		request["group_id"] = lp.GroupID
	}

	var body struct {
		Key    string `json:"key"`
		Server string `json:"server"`
		Ts     int64  `json:"ts"`
	}

	lp.Key = body.Key
	lp.Server = body.Server
	return body.Ts, nil
}