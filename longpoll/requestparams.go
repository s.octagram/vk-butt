package longpoll

import (
	"fmt"
	"net/url"
)

type RequestParams map[string]interface{}

func (params RequestParams) URLValues() (url.Values) {
	values := url.Values{}

	for k, v := range params {
		values.Add(k, fmt.Sprint(v))
	}
	return values
}