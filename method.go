package vkbutt

import (
	"encoding/json"
	"reflect"
	"gitlab.com/s.octagram/vk-butt/longpoll"
	"gitlab.com/s.octagram/vk-butt/httputil"
	"github.com/pkg/errors"
)

func (v *VkButt) MethodCall(method string, params longpoll.RequestParams, response interface{}) (error) {
	queryParams := params.URLValues()

	setIfEmpty := func(param, value string) {
		if queryParams.Get(param) == "" {
			queryParams.Set(param, value)
		}
	}
	setIfEmpty("v", v.Version)
	setIfEmpty("lang", v.Lang)

	if v.Token != "" {
		setIfEmpty("access_token", v.Token)
	}

	rawBody, err := httputil.Post(v.HTTPClient, v.BaseURL+"/"+method, queryParams)
	if err != nil {
		return err
	}

	var body struct {
		Response interface{}  `json:"response"`
		Error    *MethodError `json:"error"`
	}

	if response != nil {
		valueOfResponse := reflect.ValueOf(response)
		if valueOfResponse.Kind() != reflect.Ptr || valueOfResponse.IsNil() {
			return errors.New("response must be a valid pointer")
		}

		body.Response = response
	}

	if err = json.Unmarshal(rawBody, &body); err != nil {
		return err
	}

	if body.Error != nil {
		return body.Error
	}
	return nil
}