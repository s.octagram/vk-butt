package vkbutt

type Option func(*VkButt) error

func WithToken(token string) Option {
	return func(v *VkButt) error {
		v.Token = token
		return nil
	}
}