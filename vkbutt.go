package vkbutt

import (
	"gitlab.com/s.octagram/vk-butt/config"
	"gitlab.com/s.octagram/vk-butt/httputil"
)

type VkButt struct {
	BaseURL string
	Version string
	Lang string
	Token string
	
	HTTPClient httputil.RequestDoer

	Config *config.Config
}

func Init() (*VkButt, error) {
	return InitWithOptions()
}

func InitWithOptions(options ...Option) (*VkButt, error) {
	config := config.Init()

	vkbutt := VkButt{}
	vkbutt.BaseURL = config.DefaultBaseURL
	vkbutt.Version = config.DefaultVersion
	vkbutt.Lang = config.DefaultLang
	vkbutt.Config = config

	for _, option := range options {
		if err := option(&vkbutt); err != nil {
			return nil, err
		}
	}
	return &vkbutt, nil
}
