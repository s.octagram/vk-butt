package vkbutt

import "testing"

func TestInitWithOptions(t *testing.T) {
	token := "TOKEN"

	vkbutt, err := InitWithOptions(
		WithToken(token),
	)

	if err != nil {
		t.Error(err)
	}

	if vkbutt.Token != token {
		t.Errorf("client.token == %q, want %q", vkbutt.Token, token)
	}
}